import requests
import flet as ft

URL_BASE = "http://localhost:8000"

def generate_token (url_base, username, password):
    url = f"{url_base}/api/auth/token"
    token = requests.post(url, data={"username": username, "password": password})
    return token

def get_recipes(url_base, token):
    url = f"{url_base}/api/recipes?format=json"
    headers = {"Authorization": f"Token {token}"}
    recipes = requests.get(f"{url_base}/api/recipes?format=json",headers=headers, data={})
    return recipes.json()


def main(page: ft.Page):

    def login_click(e):
        token = generate_token(URL_BASE, username.value, password.value)
        if token.status_code == 400:
            estado_registro.value = "Usuario o contraseña incorrectos"
            page.update()
        if token.status_code == 200:
            # token = page.session.set("token", token.json()["token"])
            token = token.json()["token"]
            estado_registro.value = "Sesión Iniciada "
            recipes = get_recipes(URL_BASE, token)
            for recipe in recipes:
                lista_recetas.controls.append(ft.Text(recipe['name']))
            page.update()

    page.title = "Login"

    icono = ft.Icon(name=ft.icons.LOGIN, color=ft.colors.BLUE_300, size=50)
    username = ft.TextField(label="Usuario")
    password = ft.TextField(label="Contraseña", password=True, can_reveal_password=True)
    login_button = ft.ElevatedButton(text="Iniciar sesión", on_click=login_click)
    estado_registro = ft.Text(value="", color=ft.colors.GREEN_300)

    lista_recetas = ft.ListView(spacing=10)

    page.add(icono, username, password, login_button, estado_registro, lista_recetas)

ft.app(target=main)